// Défini l'emplacement actuel du fichier dans le programme
package fr.codingclub.crepe.block;

// Importe toutes les dépendances de l'objet actuel
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;

// Créé l'objet BlockCrepe héritant des propriétés de Block
public final class BlockCrepe extends Block {

    // La partie qui nous intéresse :
    public BlockCrepe() {
        // On défini le matériau du bloc, ici rock (pierre)
        super(Material.rock);
        // On défini le nom du bloc
        setUnlocalizedName("C4");
        // On défini l'onglet dans lequel on peut retrouver l'objet
        // en mode créatif
        setCreativeTab(CreativeTabs.tabRedstone);
        // On défini la dureté de l'object
        setHardness(1.0f);
        // On défini la résistance de l'objet
        setResistance(1.0f);
        // On défini le taux de luminosité de l'objet.
        // Quand il vaut 0 ou que la fonction n'est pas appelée
        // l'objet n'éclaire pas.
        // Ici, on spécifie que notre bloc fait de la lumière.
        setLightLevel(1.0f);
    }

    @Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumFacing side, float hitX, float hitY, float hitZ)
    {
            worldIn.createExplosion(playerIn, pos.getX(), pos.getY(), pos.getZ(), 1.0f, true);
        return false;
    }
}
