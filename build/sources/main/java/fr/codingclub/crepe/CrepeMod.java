package fr.codingclub.crepe;

import fr.codingclub.crepe.block.BlockCrepe;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.Mod;

@Mod(modid = CodingClubMod.MODID, name = CodingClubMod.MODNAME, version = CodingClubMod.MODVERSION)
public class CrepeMod extends CodingClubMod {
    public void onInit() {
        BlockCrepe crepe = new BlockCrepe();
        registerBlock(crepe, "crepe:block-crepe");
        registerRecipe(crepe, new Object[] { "###", "#P#", "###", '#', Blocks.cobblestone, 'P', Items.gunpowder });
    }
}
