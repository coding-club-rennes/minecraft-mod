// Défini l'emplacement actuel du fichier dans le programme
package fr.codingclub.crepe.block;

// Importe toutes les dépendances de l'objet actuel
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;

// Créé l'objet BlockCrepe héritant des propriétés de Block
public final class BlockCrepe extends Block {

    // La partie qui nous intéresse :
    public BlockCrepe() {
        // On défini le matériau du bloc, ici rock (pierre)
        super(Material.field_151576_e);
        // On défini le nom du bloc
        func_149663_c("C4");
        // On défini l'onglet dans lequel on peut retrouver l'objet
        // en mode créatif
        func_149647_a(CreativeTabs.field_78028_d);
        // On défini la dureté de l'object
        func_149711_c(1.0f);
        // On défini la résistance de l'objet
        func_149752_b(1.0f);
        // On défini le taux de luminosité de l'objet.
        // Quand il vaut 0 ou que la fonction n'est pas appelée
        // l'objet n'éclaire pas.
        // Ici, on spécifie que notre bloc fait de la lumière.
        func_149715_a(1.0f);
    }

    @Override
    public boolean func_180639_a(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumFacing side, float hitX, float hitY, float hitZ)
    {
            worldIn.func_72876_a(playerIn, pos.func_177958_n(), pos.func_177956_o(), pos.func_177952_p(), 1.0f, true);
        return false;
    }
}
