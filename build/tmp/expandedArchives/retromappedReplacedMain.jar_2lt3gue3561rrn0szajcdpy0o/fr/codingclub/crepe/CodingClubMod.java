package fr.codingclub.crepe;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;

abstract class CodingClubMod {
    static final String MODID = "crepe";
    static final String MODNAME = "Crepe";
    static final String MODVERSION = "1.0.0";

    void registerBlock(Block b, String name) {
        GameRegistry.registerBlock(b, name);
        Minecraft.func_71410_x()
                .func_175599_af()
                .func_175037_a()
                .func_178086_a(
                        Item.func_150898_a(b),
                        0,
                        new ModelResourceLocation(name, "inventory")
                );
    }

    void registerItem(Item item, String s) {
        GameRegistry.registerItem(item, s);
    }

    void registerRecipe(ItemStack stack, Object[] recipe) {
        GameRegistry.addRecipe(stack, recipe);
    }

    void registerRecipe(Block block, Object[] recipe) {
        registerRecipe(new ItemStack(block), recipe);
    }

    void registerRecipe(Item item, Object[] recipe) {
        registerRecipe(new ItemStack(item), recipe);
    }

    void registerSmelting(Item source, ItemStack stack) {
        GameRegistry.addSmelting(source, stack, 1.f);
    }

    void registerSmelting(Block source, ItemStack stack) {
        GameRegistry.addSmelting(source, stack, 1.f);
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent e) {
        this.onInit();
    }

    protected abstract void onInit();
}
